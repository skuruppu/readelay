/* Readelay
 * Copyright (C) 2013 Shanika Kuruppu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
Readelay implements the feature to save URLs and serve it in a form
that RSS readers can process.

Package terms implements displaying the text for the privacy policy and terms
of service.
*/
package terms

import (
    "appengine"
    "appengine/user"
    "html/template"
    "net/http"
    "session"
    "util"
)

// Handle routes.
func init() {
    http.HandleFunc("/terms", termsHandler)
}

// Handler for the user terms of service view for the service.
func termsHandler(writer http.ResponseWriter, request *http.Request) {
    context := appengine.NewContext(request)
    args := make(map[string]interface{})
    args["SiteData"] = util.GetSiteData()

    if user.Current(context) != nil {
        _, myUser, _ := session.LoggedInUser(writer, request)
        args["UserName"] = myUser
    } else {
        url, _ := user.LoginURL(context, request.URL.String())
        args["LoginURL"] = url
    }

    var baseTemplate = template.Must(template.ParseFiles(
        "templates/index.html",
        "terms/templates/terms.html",
    ))
    if err := baseTemplate.Execute(writer, args); err != nil {
        context.Errorf("%s", err.Error())
        errorMessage := "Terms and privacy policy currently unavailable."
        http.Error(writer, errorMessage, http.StatusInternalServerError)
        return
    }
}
