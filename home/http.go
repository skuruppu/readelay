/* Readelay
 * Copyright (C) 2013 Shanika Kuruppu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
Readelay implements the feature to save URLs and serve it in a form that RSS
readers can process.

Package home implements the home page for readelay.
*/
package home

import (
    "appengine"
    "appengine/user"
    "html/template"
    "net/http"
    "session"
    "util"
)

// Handle routes.
func init() {
    http.HandleFunc("/", homeHandler)
    http.HandleFunc("/logout", logoutHandler)
}

// Handler for the root. Logged in users will redirect to their view and
// non-loggedin users will see the home page with a link to login.
func homeHandler(writer http.ResponseWriter, request *http.Request) {
    context := appengine.NewContext(request)
    myUser := user.Current(context)

    // User already logged in so redirect to their view
    if myUser != nil {
        writer.Header().Set("Location", "/list")
        writer.WriteHeader(http.StatusFound)
        return
    }

    errorMessage := "Readelay home page currently unavailable."

    url, err := user.LoginURL(context, request.URL.String())
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, errorMessage, http.StatusInternalServerError)
        return
    }

    args := make(map[string]interface{})
    args["LoginURL"] = url
    args["SiteData"] = util.GetSiteData()

    var baseTemplate = template.Must(template.ParseFiles(
        "templates/index.html",
        "home/templates/home.html",
    ))

    if err := baseTemplate.Execute(writer, args); err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, errorMessage, http.StatusInternalServerError)
        return
    }

    return
}

// Handler for logging out users.
func logoutHandler(writer http.ResponseWriter, request *http.Request) {
    context := appengine.NewContext(request)
    myUser := user.Current(context)

    if myUser == nil {
        return
    }

    url, err := user.LogoutURL(context, "/")
    if err != nil {
        context.Errorf("%s", err.Error())
        errorMessage := "Unable to logout."
        http.Error(writer, errorMessage, http.StatusInternalServerError)
        return
    }

    session.ClearSession(writer, request)

    writer.Header().Set("Location", url)
    writer.WriteHeader(http.StatusFound)
    return
}
