var readelayApp = angular.module('readelayApp', [
    'ngResource', 'ui.bootstrap'
]);

readelayApp.constant('CONSTANTS', CONSTANTS);

readelayApp.factory('Items', ['$resource', function($resource) {
    return $resource('/items/:id',
        {
            id: '@id'
        },
        {
            'get': {method: 'GET'},
            'delete': {method: 'DELETE'},
            'update': {method: 'PUT'},
            'save': {method: 'POST'}
        }
    );
}]);

readelayApp.controller('ItemErrorCtrl', [
    '$scope', '$rootScope', '$location', '$anchorScroll',
    function($scope, $rootScope, $location, $anchorScroll) {
        $scope.message = '';

        $rootScope.$on('ItemErrorShow', function(e, message) {
            $scope.message = message;
            $location.hash('item_error');
            $anchorScroll();
        });

        $rootScope.$on('ItemErrorClear', function(e) {
            $scope.close();
        });

        $scope.close = function() {
            $scope.message = '';
        };
    }
]);

readelayApp.controller('ListCtrl', [
    '$scope', '$rootScope', '$http', '$filter', 'Items', 'CONSTANTS',
    function ListCtrl($scope, $rootScope, $http, $filter, Items, CONSTANTS) {
        $scope.new_item = {
            'URL': '',
            'Title': '',
            'Description': ''
        };

        // Constants for validating new items
        $scope.constants = CONSTANTS;

        $scope.submitting = false;
        $scope.fetched = false;

        $scope.old_search_query = '';
        $scope.search_query = '';

        function query(params) {
            $rootScope.$emit('ItemErrorClear');
            Items.query(params,
                function(response) {
                    $scope.unread_items = $filter('filter')(
                        response, {IsRead: false}
                    );
                    $scope.read_items = $filter('filter')(
                        response, {IsRead: true}
                    );
                },
                function(response) {
                    $rootScope.$emit('ItemErrorShow', response.data);
                }
            );
        }

        query();

        $scope.fetch = function(url) {
            $rootScope.$emit('ItemErrorClear');
            $scope.submitting = true;
            $http.post('fetch', {'URL': url}).
                success(function(response, status) {
                    $scope.submitting = false;
                    $scope.fetched = true;
                    $scope.new_item = response;
                }).error(function(response, status) {
                    $rootScope.$emit('ItemErrorShow', response);
                    $scope.submitting = false;
                })
        }

        $scope.add = function(item, item_form) {
            $rootScope.$emit('ItemErrorClear');
            $scope.submitting = true;
            Items.save(item, function(response) {
                // Add new item to the start of the unread list.
                $scope.unread_items.unshift(response);
                $scope.reset(item_form);
                $scope.submitting = false;
            }, function(response) {
                $rootScope.$emit('ItemErrorShow', response.data);
                $scope.submitting = false;
            })
        }

        $scope.delete = function(item, index) {
            $rootScope.$emit('ItemErrorClear');
            Items.delete({id: item.ID}, function() {
                var items;
                if (item.IsRead) {
                    items = $scope.read_items;
                } else {
                    items = $scope.unread_items;
                }
                items.splice(index, 1);
            }, function(response) {
                $rootScope.$emit('ItemErrorShow', response.data);
            });
        }

        $scope.toggle_read_state = function(item, index) {
            $rootScope.$emit('ItemErrorClear');
            item.IsRead = !item.IsRead;
            Items.update({id: item.ID}, item,
                function() {
                    var old_items, new_items;
                    if (item.IsRead) {
                        new_items = $scope.read_items;
                        old_items = $scope.unread_items;
                    } else {
                        new_items = $scope.unread_items;
                        old_items = $scope.read_items;
                    }
                    new_items.unshift(item);
                    old_items.splice(index, 1);
                },
                function(response) {
                    item.IsRead = !item.IsRead;
                    $rootScope.$emit('ItemErrorShow', response.data);
                }
            );
        }

        $scope.reset = function(item_form) {
            $scope.new_item.URL = '';
            $scope.new_item.Title = '';
            $scope.new_item.Description = '';
            item_form.$setPristine();

            $scope.fetched = false;
            $scope.submitting = false;
        }

        $scope.focus_add = function() {
            $scope.$emit('Focus');
        }

        $scope.search_change = function() {
            // Only trigger a search on change if the search input box is
            // cleared.
            if ($scope.search_query == '') {
                $scope.search();
            }
        }

        $scope.search = function() {
            // Only search if current search query is not the same as the old
            // search query.
            if ($scope.old_search_query !== $scope.search_query) {
                query({query: $scope.search_query});
                $scope.old_search_query = $scope.search_query;
            }
        }
    }
]);

readelayApp.directive('focusInput', function($rootScope) {
    return function(scope, element, attrs) {
        $rootScope.$on('Focus', function() {
            element[0].focus();
        });
    }
});

// reading-list directive that renders a reading list tab. The directive
// requires the list of items to be rendered and the functions to toggle the
// reading state of an item and to delete an item.
readelayApp.directive('readingList', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'templates/items.html',
        scope: {
            items: '=',
            toggle: '=',
            delete: '='
        }
    };
});

readelayApp.factory('Settings', ['$resource', function($resource) {
    return $resource('/rss/settings/ ', {},
        {
            'get': {method: 'GET'},
            'update': {method: 'PUT'}
        }
    );
}]);

readelayApp.controller('SettingsCtrl', [
    '$scope', '$modal', '$http', 'Settings',
    function SettingsCtrl($scope, $modal, $http, Settings) {
        var SettingsInstanceCtrl = function(
            $scope, $modalInstance, settings, save, messages) {
            $scope.settings = settings;

            $scope.save = save;

            $scope.messages = messages;

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        };

        $scope.settings = Settings.get(
            function(response) {
            }, function(response) {
                $scope.messages.error = response.data;
            }
        );

        $scope.open = function() {
            $scope.message_reset();

            var modalInstance = $modal.open({
                templateUrl: 'templates/settings.html',
                controller: SettingsInstanceCtrl,
                resolve: {
                    settings: function() {
                        return $scope.settings;
                    },
                    save: function() {
                        return $scope.save;
                    },
                    messages: function() {
                        return $scope.messages;
                    }
                }
            });
        };

        $scope.save = function() {
            $scope.message_reset();

            Settings.update($scope.settings, function(response) {
                $scope.messages.success = 'Settings successfully saved!';
            }, function(response) {
                $scope.messages.error = response.data;
            });
        };

        $scope.messages = {};

        $scope.message_reset = function() {
            $scope.messages.success = '';
            $scope.messages.error = '';
        };

        $scope.message_reset();
    }
]);
