/* Readelay
 * Copyright (C) 2013 Shanika Kuruppu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
Readelay implements the feature to save URLs and serve it in a form
that RSS readers can process.

Package rss implements the rss feed for a user.
*/
package rss

import (
    "appengine"
    "appengine/datastore"
    "encoding/json"
    "encoding/xml"
    "github.com/gorilla/mux"
    "github.com/nu7hatch/gouuid"
    "io/ioutil"
    "net/http"
    "session"
    "strconv"
    "strings"
    "time"
    "util"
)

// Handle routes.
func init() {
    router := mux.NewRouter()
    router.HandleFunc("/rss/", rssHandler).Methods("GET")
    router.HandleFunc("/rss/settings/", rssSettingsGetHandler).Methods("GET")
    router.HandleFunc("/rss/settings/", rssSettingsPutHandler).Methods("PUT")
    http.Handle("/rss/", router)
}

type Feed struct {
    XMLName xml.Name  `xml:"http://www.w3.org/2005/Atom feed"`
    Title   string    `xml:"title"`
    Id      string    `xml:"id"`
    Link    []Link    `xml:"link"`
    Updated time.Time `xml:"updated,attr"`
    Author  Person    `xml:"author"`
    Entry   []Entry   `xml:"entry"`
}

type Entry struct {
    Title   string    `xml:"title"`
    ID      string    `xml:"id"`
    Link    []Link    `xml:"link"`
    Updated time.Time `xml:"updated"`
    Summary Text      `xml:"summary"`
}

type Link struct {
    Rel  string `xml:"rel,attr,omitempty"`
    Href string `xml:"href,attr"`
}

type Person struct {
    Name string `xml:"name"`
}

type Text struct {
    Type string `xml:"type,attr,omitempty"`
    Body string `xml:",chardata"`
}

const MAXTITLELEN = 256

type RSSSettings struct {
    RSSID       string
    PublishRSS  bool
    RSSTitle    string
    RSSURL      string
}

// Handler for the user rss view of their reading list.
func rssHandler(writer http.ResponseWriter, request *http.Request) {
    context := appengine.NewContext(request)

    id := strings.TrimSpace(request.FormValue("id"))
    // Redirect to home page if there is no RSS ID provided
    if id == "" {
        writer.Header().Set("Location", "/")
        writer.WriteHeader(http.StatusFound)
        return
    }

    errorMessage := "RSS feed is not available."

    query := datastore.NewQuery("ReadelayUser").
        Filter("RSSID =", id)
    var users []util.ReadelayUser

    _, err := query.GetAll(context, &users)
    if err != nil || len(users) != 1 {
        context.Errorf("%s", err.Error())
        http.Error(writer, errorMessage, http.StatusInternalServerError)
        return
    }

    myUser := users[0]
    // Not allowed to publish RSS feed
    if myUser.PublishRSS == false {
        http.Error(writer, errorMessage, http.StatusForbidden)
        return
    }

    filter := util.ItemFilter{State: util.ItemStateUnread}
    itemsView, err := util.GetReadingList(context, myUser.ID, filter)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, errorMessage, http.StatusInternalServerError)
        return
    }

    entries := make([]Entry, len(itemsView))
    for i, item := range itemsView {
        entryUuid, err := uuid.NewV5(uuid.NamespaceURL, []byte(item.ID))
        if err != nil {
            context.Errorf("%s", err.Error())
            http.Error(writer, errorMessage, http.StatusInternalServerError)
            return
        }

        entries[i].Title = item.Title
        entries[i].ID = "urn:uuid:" + entryUuid.String()
        entries[i].Link = []Link{{
            Rel:  "alternate",
            Href: item.URL},
        }
        entries[i].Updated = item.Time
        entries[i].Summary = Text{Type: "text", Body: item.Description}
    }

    var updatedTime time.Time
    if len(entries) > 0 {
        updatedTime = itemsView[0].Time
    } else {
        updatedTime = time.Now()
    }

    userUuid, err := uuid.NewV5(uuid.NamespaceURL, []byte(myUser.ID))
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, errorMessage, http.StatusInternalServerError)
        return
    }

    rssTitle := util.DefaultRSSTitle
    if myUser.RSSTitle != "" {
        rssTitle = myUser.RSSTitle
    }

    var atomFeed = Feed{
        XMLName: xml.Name{
            Space: "http://www.w3.org/2005/Atom",
            Local: "feed",
        },
        Title: rssTitle,
        Link: []Link{
            {Rel: "alternate", Href: util.SiteURL},
            {Rel: "self", Href: util.SiteURL + "/rss?id=" + myUser.RSSID},
        },
        Id:      "urn:uuid:" + userUuid.String(),
        Updated: updatedTime,
        Author: Person{
            Name: util.SiteName,
        },
        Entry: entries,
    }

    writer.Write([]byte("<?xml version=\"1.0\" encoding=\"utf-8\"?>"))
    encoder := xml.NewEncoder(writer)
    encoder.Indent("  ", "    ")
    if err := encoder.Encode(atomFeed); err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, errorMessage, http.StatusInternalServerError)
        return
    }
}


// Handler for fetching user RSS settings.
func rssSettingsGetHandler(writer http.ResponseWriter, request *http.Request) {
    _, myUser, appUser := session.VerifyRequest(writer, request)
    if myUser == nil {
        return
    }

    settings := RSSSettings {
        RSSID:      appUser.RSSID,
        PublishRSS: appUser.PublishRSS,
        RSSTitle:   appUser.RSSTitle,
        RSSURL:     util.SiteURL + "/rss/?id=" + appUser.RSSID,
    }

    util.SendJSONResponse(settings, writer)
}

// Handler for updating user RSS settings.
func rssSettingsPutHandler(writer http.ResponseWriter, request *http.Request) {
    context, myUser, appUser := session.VerifyRequest(writer, request)
    if myUser == nil {
        return
    }

    message := "We couldn't save your RSS settings."

    // AngularJS sends PUT and POST request data as
    // content-type: application/json. Therefore we have to decode the request
    // body ourselves.

    body, err := ioutil.ReadAll(request.Body)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    settings := RSSSettings{}
    err = json.Unmarshal([]byte(body), &settings)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    appUser.PublishRSS = settings.PublishRSS

    appUser.RSSTitle = strings.TrimSpace(settings.RSSTitle)
    if len(appUser.RSSTitle) >= MAXTITLELEN {
        message = "Title must be at most " +
            strconv.Itoa(MAXTITLELEN) + " characters."
        http.Error(writer, message, http.StatusBadRequest)
        return
    }

    key := datastore.NewKey(context, "ReadelayUser", appUser.ID, 0, nil)
    _, err = datastore.Put(context, key, appUser)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    return
}
