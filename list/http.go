/* Readelay
 * Copyright (C) 2013 Shanika Kuruppu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
Readelay implements the feature to save URLs and serve it in a form
that RSS readers can process.

Package list implements the list view of the users links.
*/
package list

import (
    "appengine"
    "appengine/datastore"
    "appengine/urlfetch"
    "appengine/user"
    "bytes"
    "code.google.com/p/go.net/html"
    "encoding/json"
    "html/template"
    "io/ioutil"
    "net/http"
    "session"
    "strconv"
    "strings"
    "time"
    "util"
)

// Handle routes.
func init() {
    http.HandleFunc("/list", listHandler)
    http.HandleFunc("/fetch", fetchHandler)
}

// Handler for the user view which is a list of links.
func listHandler(writer http.ResponseWriter, request *http.Request) {
    context, myUser, appUser := session.LoggedInUser(writer, request)
    if myUser == nil {
        return
    }

    session.SetXSRFCookie(writer, appUser)

    constants := make(map[string]interface{})
    constants["MAXURLLEN"] = util.MAXURLLEN
    constants["MAXTITLELEN"] = util.MAXTITLELEN
    constants["MAXDESCLEN"] = util.MAXDESCLEN

    args := make(map[string]interface{})
    args["UserName"] = myUser
    args["Constants"] = constants
    args["SiteData"] = util.GetSiteData()

    var baseTemplate = template.Must(template.ParseFiles(
        "templates/index.html",
        "list/templates/list.html",
    ))
    if err := baseTemplate.Execute(writer, args); err != nil {
        errorMessage := "Sorry, your reading list is currently unavailable."
        context.Errorf("%s", err.Error())
        http.Error(writer, errorMessage, http.StatusInternalServerError)
        return
    }
}

func fetchHandler(writer http.ResponseWriter, request *http.Request) {
    context, myUser, _ := session.VerifyRequest(writer, request)
    if myUser == nil {
        return
    }

    message := "We couldn't fetch your URL."

    item := util.ReadItemView{
        ID:          "",
        Title:       "",
        Description: "",
        URL:         "",
        Time:        time.Now(),
    }

    // AngularJS sends POST request data as content-type: application/json.
    // Therefore we have to decode the request body ourselves.

    body, err := ioutil.ReadAll(request.Body)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    err = json.Unmarshal([]byte(body), &item)
    if err != nil {
        context.Errorf("%s", err.Error())
        util.SendJSONResponse(item, writer)
        return
    }

    item.URL = strings.TrimSpace(item.URL)
    if len(item.URL) == 0 {
        message = "URL can't be empty."
        http.Error(writer, message, http.StatusBadRequest)
        return
    }

    if len(item.URL) > util.MAXURLLEN {
        message = "URL must be shorter than " +
            strconv.Itoa(util.MAXURLLEN) + " characters."
        http.Error(writer, message, http.StatusBadRequest)
        return
    }

    isDuplicate, _ := isDuplicateURL(context, myUser, item.URL)
    if isDuplicate == true {
        message = "This item is already in your reading list."
        http.Error(writer, message, http.StatusBadRequest)
        return
    }

    title, description, err := fetchURLContent(context, writer, item.URL)
    if err != nil {
        context.Errorf("%s", err.Error())
        util.SendJSONResponse(item, writer)
        return
    }
    item.Title = title
    item.Description = description

    util.SendJSONResponse(item, writer)
}

// Fetches the content from a given URL and parses it to return a title of the
// HTML document and a description based on content of first p tag.
func fetchURLContent(context appengine.Context, writer http.ResponseWriter,
    url string) (title string, description string, err error) {
    title = ""
    description = ""

    client := urlfetch.Client(context)
    response, err := client.Get(url)
    if err != nil {
        return title, description, err
    }

    body, err := html.Parse(response.Body)
    if err != nil {
        return title, description, err
    }
    response.Body.Close()

    titleNode := getFirstNodeOfType(body, "title")
    if titleNode != nil {
        title = strings.TrimSpace(extractText(titleNode))
        if len(title) > util.MAXTITLELEN {
            title = title[:util.MAXTITLELEN-3] + "..."
        }
    }

    descriptionNode := getFirstNodeOfType(body, "p")
    if descriptionNode != nil {
        description = strings.TrimSpace(extractText(descriptionNode))
        if len(description) > util.MAXDESCLEN {
            description = description[:util.MAXDESCLEN-3] + "..."
        }
    }

    return title, description, nil
}

// Given a root DOM tree node, returns the first node encountered of the given
// type. If no node of the given type is found, nil is returned.
func getFirstNodeOfType(node *html.Node,
    nodeType string) (retNode *html.Node) {
    if node.Type == html.ElementNode && node.Data == nodeType {
        return node
    }

    for child := node.FirstChild; child != nil; child = child.NextSibling {
        returnNode := getFirstNodeOfType(child, nodeType)
        if returnNode != nil {
            return returnNode
        }
    }

    return nil
}

// Given a root DOM node, returns the recursively extracted text from the
// child nodes of the root node.
func extractText(node *html.Node) (text string) {
    var buffer bytes.Buffer
    extractTextRecursive(node, &buffer)

    return buffer.String()
}

// Recursive helper for extractText.
func extractTextRecursive(node *html.Node, text *bytes.Buffer) {
    if node.Type == html.TextNode {
        text.WriteString(node.Data)
        return
    }

    for child := node.FirstChild; child != nil; child = child.NextSibling {
        extractTextRecursive(child, text)
    }

    return
}

// Checks if the URL was already saved for the given user.
func isDuplicateURL(context appengine.Context, myUser *user.User,
    url string) (bool, error) {
    query := datastore.NewQuery("ReadItem").
        Filter("UserID =", myUser.ID).
        Filter("URL =", url)

    count, err := query.Count(context)
    if err != nil {
        return false, err
    }

    return count != 0, nil
}
