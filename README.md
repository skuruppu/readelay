Readelay is a web application that allows you to save your internet reading
material to read at a later and more convenient time for you.  Your reading
list can be accessed as an RSS feed so that you can read your material through
your favourite RSS reader.

This is a Google App Engine application written in Go and is currently
available at:

[http://readelay.appspot.com](http://readelay.appspot.com)


Third-Party Packages
====================

This program relies on two third-party packages for HTML parsing and
generating UUIDs, respectively. Install these packages with `go get` as
follows:

    go get github.com/nu7hatch/gouuid

    go get code.google.com/p/go.net/html

    go get github.com/gorilla/mux

Note that the html package installs other packages for dealing with sockets,
etc. that the restricted Google App Engine go compiler will now allow you to
compile. Therefore you may need to remove those files in the packages you
upload to App Engine for hosting.

Distribution
============

Refer to the `COPYING` file for licensing information of this program.
