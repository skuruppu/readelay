/* Readelay
 * Copyright (C) 2013 Shanika Kuruppu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
Readelay implements the feature to save URLs and serve it in a form that RSS
readers can process.

Package util implements utility structs, constants and functions for the
application.
*/
package util

import (
    "appengine"
    "appengine/datastore"
    "encoding/json"
    "fmt"
    "net/http"
    "time"
)

type ReadelayUser struct {
    ID         string    // Google user ID
    SignupTime time.Time // Time the user signed up to service
    RSSID      string    // Unique ID for user RSS feed
    PublishRSS bool      // Whether to publish RSS feed
    RSSTitle   string    // Title for RSS feed
    FormToken  string    // Token for submitting forms
    AppToken   string    // Token for submitting forms through apps
}

type ReadItem struct {
    Title       string
    Description string
    URL         string
    Time        time.Time
    UserID      string
    IsRead      bool
}

type ReadItemView struct {
    ID          string
    Title       string
    Description string
    URL         string
    Time        time.Time
    IsRead      bool
}

type ItemState uint8

const (
    ItemStateRead ItemState = iota
    ItemStateUnread
    ItemStateAll
)

type ItemFilter struct {
    State   ItemState
    Keys    []*datastore.Key
}

// Fetches the reading items that match the given filter. If a list of
// datastore keys are specified then the remaining filter fields are ignored.
func GetReadingList(context appengine.Context,
    userID string, itemFilter ItemFilter) ([]ReadItemView, error) {
    var items []ReadItem
    var err error
    var keys []*datastore.Key

    if itemFilter.Keys != nil {
        items = make([]ReadItem, len(itemFilter.Keys))
        err = datastore.GetMulti(context, itemFilter.Keys, items)
        keys = itemFilter.Keys
    } else {
        query := datastore.NewQuery("ReadItem").
            Filter("UserID =", userID).
            Order("-Time")

        // Only fetch items for the given read state
        if itemFilter.State != ItemStateAll {
            isRead := itemFilter.State == ItemStateRead
            query = query.Filter("IsRead =", isRead)
        }

        keys, err = query.GetAll(context, &items)
    }

    if err != nil {
        context.Errorf("%s", err.Error())
        return nil, err
    }

    itemsView := make([]ReadItemView, len(items))
    for i, item := range items {
        itemsView[i].ID = keys[i].Encode()
        itemsView[i].Title = item.Title
        itemsView[i].Description = item.Description
        itemsView[i].URL = item.URL
        itemsView[i].Time = item.Time
        itemsView[i].IsRead = item.IsRead
    }

    return itemsView, nil
}

// Sends a JSON response.
func SendJSONResponse(response interface{},
    writer http.ResponseWriter) {
    responseJSON, err := json.Marshal(response)
    if err != nil {
        message := "Could not encode JSON response."
        http.Error(writer, message, http.StatusInternalServerError)
    } else {
        fmt.Fprintf(writer, "%s", string(responseJSON))
    }
}

func GetSiteData() (map[string]interface{}) {
    siteData := make(map[string]interface{})
    siteData["SiteName"] = SiteName
    siteData["SiteSlogan"] = SiteSlogan
    siteData["SiteURL"] = SiteURL

    return siteData
}
