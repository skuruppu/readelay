/* Readelay
 * Copyright (C) 2013 Shanika Kuruppu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
Readelay implements the feature to save URLs and serve it in a form that RSS
readers can process.

Package util implements utility structs, constants and functions for the
application.
*/
package util

const SiteName = "Readelay"
const SiteSlogan = "Save for later!"
const SiteURL = "http://readelay.appspot.com"

const XSRFCookie = "XSRF-TOKEN"
const XSRFHeader = "X-XSRF-TOKEN"

const DefaultRSSTitle = "My " + SiteName + " Reading List"

// http://stackoverflow.com/questions/417142/what-is-the-maximum-length-of-a-url-in-different-browsers
const MAXURLLEN = 500
const MAXTITLELEN = 200
const MAXDESCLEN = 500
