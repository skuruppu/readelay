/* Readelay
 * Copyright (C) 2013 Shanika Kuruppu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
Readelay implements the feature to save URLs and serve it in a form
that RSS readers can process.

Package tasks implements tasks that need to be processed in the background.
*/
package tasks

import (
    "appengine"
    "appengine/datastore"
    "appengine/taskqueue"
    "net/http"
    "search"
    "strings"
    "util"
)

// Handle routes.
func init() {
    http.HandleFunc("/tasks", taskHandler)
    http.HandleFunc("/index", indexHandler)
}

// Adds the task matching the given taskName to the task queue. The task will
// only be handled if the name and input parameters are valid for the task.
func taskHandler(writer http.ResponseWriter, request *http.Request) {
    context := appengine.NewContext(request)

    taskName := request.FormValue("taskName")
    if taskName == "" {
        return
    }

    var err error

    if taskName == "index" {
        task := taskqueue.NewPOSTTask("/index", request.Form)
        _, err = taskqueue.Add(context, task, "")
    }

    if err != nil {
        http.Error(writer, err.Error(), http.StatusInternalServerError)
        return
    }
}

// Handler for indexing items for full text search. If a userID parameter is
// provided, only indexes the items for that user. Otherwise indexes items for
// all users.
func indexHandler(writer http.ResponseWriter, request *http.Request) {
    context := appengine.NewContext(request)

    query := datastore.NewQuery("ReadItem")

    userID := strings.TrimSpace(request.FormValue("userID"))
    if userID != "" {
        query = query.Filter("UserID =", userID)
    }

    var items []util.ReadItem
    keys, err := query.GetAll(context, &items)
    if err != nil {
        http.Error(writer, err.Error(), http.StatusInternalServerError)
        return
    }

    indexItems := make(map[string]util.ReadItem)
    for i, item := range items {
        indexItems[keys[i].Encode()] = item
    }

    err = search.AddItems(context, indexItems)
    if err != nil {
        http.Error(writer, err.Error(), http.StatusInternalServerError)
        return
    }
}
