/* Readelay
 * Copyright (C) 2013 Shanika Kuruppu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
Readelay implements the feature to save URLs and serve it in a form
that RSS readers can process.

Package items implements the REST API for managing reading items.
*/
package items

import (
    "appengine/datastore"
    "encoding/json"
    "errors"
    "github.com/gorilla/mux"
    "io/ioutil"
    "net/http"
    "search"
    "session"
    "strconv"
    "strings"
    "time"
    "util"
)

// Handle routes.
func init() {
    router := mux.NewRouter()
    router.HandleFunc("/items", itemsGetHandler).Methods("GET")
    router.HandleFunc("/items", itemPostHandler).Methods("POST")
    http.Handle("/items", router)

    // Need to separate the routers. /items/id PUT and DELETE requests return
    // a 302 without StrictSlash enabled. But /items POST redirects to /items
    // GET with StrictSlash.
    // TODO: Figure out why /items/id causes a 302 without StrictSlash.
    slashRouter := mux.NewRouter()
    slashRouter.StrictSlash(true)
    slashRouter.HandleFunc("/items/{item_id}",
        itemDeleteHandler).Methods("DELETE")
    slashRouter.HandleFunc("/items/{item_id}", itemPutHandler).Methods("PUT")
    http.Handle("/items/", slashRouter)
}

// Handler for the items GET request which fetches the logged in user's
// reading items. Items can be filtered based on the following parameters:
// * query: Returns only items containing the query string (optional).
// * state: Returns items matching given state {read, unread} (optional).
func itemsGetHandler(writer http.ResponseWriter, request *http.Request) {
    context, myUser, _ := session.VerifyRequest(writer, request)
    if myUser == nil {
        return
    }

    filter := util.ItemFilter{}
    message := "Your reading list is currently unavailable."

    query := strings.TrimSpace(request.FormValue("query"))
    if query != "" {
        documentIDs, err := search.SearchItems(context, query)
        if err != nil {
            http.Error(writer, "Invalid search query.",
                http.StatusInternalServerError)
            return
        }

        filter.Keys = make([]*datastore.Key, len(documentIDs))
        for i, documentID := range documentIDs {
            filter.Keys[i], err = datastore.DecodeKey(documentID)
            if err != nil {
                http.Error(writer, message, http.StatusInternalServerError)
                return
            }
        }
    } else {
        state := strings.TrimSpace(request.FormValue("state"))
        stateMap := map[string]util.ItemState {
            "": util.ItemStateAll,
            "unread": util.ItemStateUnread,
            "read": util.ItemStateRead,
        }
        filter.State = stateMap[state]
    }

    items, err := util.GetReadingList(context, myUser.ID, filter)
    if err != nil {
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    util.SendJSONResponse(items, writer)
}

// Handler for the items POST request which adds a given item to the user's
// reading items.
func itemPostHandler(writer http.ResponseWriter, request *http.Request) {
    context, myUser, _ := session.VerifyRequest(writer, request)
    if myUser == nil {
        return
    }

    message := "We couldn't save your reading item."

    // AngularJS sends PUT and POST request data as
    // content-type: application/json. Therefore we have to decode the request
    // body ourselves.

    body, err := ioutil.ReadAll(request.Body)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    item := util.ReadItem{}
    err = json.Unmarshal([]byte(body), &item)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    item.Title = strings.TrimSpace(item.Title)
    item.Description = strings.TrimSpace(item.Description)
    item.URL = strings.TrimSpace(item.URL)
    item.IsRead = false
    item.Time = time.Now()
    item.UserID = myUser.ID

    err = validator(item)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, err.Error(), http.StatusBadRequest)
        return
    }

    // Add to datastore
    key, err := datastore.Put(context,
        datastore.NewIncompleteKey(context, "ReadItem", nil), &item)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    // Add to full text search index
    err = search.AddItems(context, map[string]util.ReadItem{key.Encode(): item})
    if err != nil {
        context.Errorf("%s", err.Error())
    }

    itemView := util.ReadItemView{
        ID:          key.Encode(),
        Title:       item.Title,
        Description: item.Description,
        URL:         item.URL,
        Time:        item.Time,
        IsRead:      item.IsRead,
    }

    util.SendJSONResponse(itemView, writer)
}

// Handler for deleting a reading item.
func itemDeleteHandler(writer http.ResponseWriter, request *http.Request) {
    context, myUser, _ := session.VerifyRequest(writer, request)
    if myUser == nil {
        return
    }

    message := "We couldn't delete your reading item."

    id := strings.TrimSpace(mux.Vars(request)["item_id"])
    key, err := datastore.DecodeKey(id)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    err = datastore.Delete(context, key)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    search.DeleteItem(context, id)

    return
}

// Handler for updating a reading item. Only the title, description, URL and
// the read status of a reading item can be updated. Any other changes will be
// ignored.
func itemPutHandler(writer http.ResponseWriter, request *http.Request) {
    context, myUser, _ := session.VerifyRequest(writer, request)
    if myUser == nil {
        return
    }

    message := "We couldn't update your reading item."

    id := strings.TrimSpace(mux.Vars(request)["item_id"])

    // AngularJS sends PUT and POST request data as
    // content-type: application/json. Therefore we have to decode the request
    // body ourselves.

    body, err := ioutil.ReadAll(request.Body)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    itemView := util.ReadItemView{}
    err = json.Unmarshal([]byte(body), &itemView)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    key, err := datastore.DecodeKey(id)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    item := util.ReadItem{}
    err = datastore.Get(context, key, &item)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    item.Title = strings.TrimSpace(itemView.Title)
    item.Description = strings.TrimSpace(itemView.Description)
    item.URL = strings.TrimSpace(itemView.URL)
    item.IsRead = itemView.IsRead

    // Validate the item parameters that can be modified through this request
    err = validator(item)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, err.Error(), http.StatusBadRequest)
        return
    }

    // Update entry in datastore
    key, err = datastore.Put(context, key, &item)
    if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, message, http.StatusInternalServerError)
        return
    }

    // Update document in full text search index
    search.AddItems(context, map[string]util.ReadItem{key.Encode(): item})

    return
}

// Validates the Title, Description and URL fields of a reading item.
func validator(item util.ReadItem) (error) {
    if len(item.Title) == 0 || len(item.URL) == 0 {
        return errors.New("Title and URL can't be empty.")
    }

    if len(item.Title) > util.MAXTITLELEN {
        return errors.New("Title must be shorter than " +
            strconv.Itoa(util.MAXTITLELEN) + " characters.")
    }

    if len(item.Description) > util.MAXDESCLEN {
        return errors.New("Description must be shorter than " +
            strconv.Itoa(util.MAXDESCLEN) + " characters.")
    }

    if len(item.URL) > util.MAXURLLEN {
        return errors.New("URL must be shorter than " +
            strconv.Itoa(util.MAXURLLEN) + " characters.")
    }

    return nil
}
