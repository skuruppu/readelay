/* Readelay
 * Copyright (C) 2013 Shanika Kuruppu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
Readelay implements the feature to save URLs and serve it in a form
that RSS readers can process.

Package search implements methods for supporting item search.
*/
package search

import (
    "appengine"
    "appengine/search"
    "errors"
    "time"
    "util"
)

type SearchReadItem struct {
    Title       string
    Description string
    URL         string
    Time        time.Time
    UserID      string
}

const IndexName = "ReadItems"

// Adds the given items to the full text search index.
func AddItems(context appengine.Context, items map[string]util.ReadItem) (error) {
    index, err := search.Open(IndexName)
    if err != nil {
        return err
    }

    failCount := 0

    for documentID, item := range items {
        searchItem := SearchReadItem{
            Title: item.Title,
            Description: item.Description,
            URL: item.URL,
            Time: item.Time,
            UserID: item.UserID,
        }

        _, err = index.Put(context, documentID, &searchItem)
        if err != nil {
            context.Errorf("%s", err.Error())
            // Continue in case it's a one off failure
            failCount += 1
        }
    }

    // None of the items were indexed
    if failCount == len(items) {
        return errors.New("Could not index the items.")
    }

    return nil
}

// Deletes the item with the given document ID.
func DeleteItem(context appengine.Context, documentID string) (error) {
    index, err := search.Open(IndexName)
    if err != nil {
        return err
    }

    err = index.Delete(context, documentID)
    if err != nil {
        context.Errorf("%s", err.Error())
        return errors.New("Unable to delete the document.")
    }

    return nil
}

// Searches the index for the given query. Returns a list of document IDs
// of reading items containing the search text. Currently only supports
// searching for strings in the URL, Title and Description fields.
func SearchItems(context appengine.Context, text string) ([]string, error) {
    message := "Error occurred during search."

    index, err := search.Open(IndexName)
    if err != nil {
        context.Errorf("%s", err.Error())
        return nil, errors.New(message)
    }

    query := "URL: " + text + " OR Title: " + text + " OR Description: " + text
    var matches []string
    for match := index.Search(context, query, nil); ; {
        var item SearchReadItem
        documentID, err := match.Next(&item)

        if err == search.Done {
            break
        }

        if err != nil {
            context.Errorf("%s", err.Error())
            return nil, errors.New(message)
        }

        matches = append(matches, documentID)
    }

    return matches, nil
}
