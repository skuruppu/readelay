/* Readelay
 * Copyright (C) 2013 Shanika Kuruppu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
Readelay implements the feature to save URLs and serve it in a form that RSS
readers can process.

Package session implements utility functions for managing sessions.
*/
package session

import (
    "appengine"
    "appengine/datastore"
    "appengine/user"
    "crypto/rand"
    "crypto/sha256"
    "encoding/base64"
    "errors"
    "net/http"
    "time"
    "util"
)

// Checks if a user is logged in and if not redirects to home page. Otherwise
// returns the context and user objects.
func LoggedInUser(writer http.ResponseWriter,
    request *http.Request) (appengine.Context, *user.User,
    *util.ReadelayUser) {
    context := appengine.NewContext(request)
    myUser := user.Current(context)

    // User not logged in so redirect to home page
    if myUser == nil {
        http.Redirect(writer, request, "/", http.StatusFound)
        return context, nil, nil
    }

    key := datastore.NewKey(context, "ReadelayUser", myUser.ID, 0, nil)
    appUser := util.ReadelayUser{}
    err := datastore.Get(context, key, &appUser)

    errorMessage := "There was an error authenticating your account."

    // New user so save an entry
    if err == datastore.ErrNoSuchEntity {
        appUser.ID = myUser.ID
        err := newAccount(context, &appUser)
        if err != nil {
            context.Errorf("%s", err.Error())
            http.Error(writer, errorMessage, http.StatusForbidden)
            return context, myUser, nil
        }
    } else if err != nil {
        context.Errorf("%s", err.Error())
        http.Error(writer, errorMessage, http.StatusForbidden)
        return context, nil, nil
    }

    if appUser.RSSTitle == "" {
        appUser.RSSTitle = util.DefaultRSSTitle
    }

    return context, myUser, &appUser
}

// Verifies the user request by checking if user is logged in and the XSRF
// token is valid.
func VerifyRequest(writer http.ResponseWriter,
    request *http.Request) (appengine.Context, *user.User,
    *util.ReadelayUser) {
    context, myUser, appUser := LoggedInUser(writer, request)
    if myUser == nil {
        return context, nil, nil
    }

    token := request.Header.Get(util.XSRFHeader)
    if appUser.FormToken != token {
        message := "There was an error authenticating your account."
        http.Error(writer, message, http.StatusForbidden)
        return context, nil, nil
    }

    return context, myUser, appUser
}

// Creates an XSRF cookie
func SetXSRFCookie(writer http.ResponseWriter, appUser *util.ReadelayUser) {
    expiration := time.Now()
    expiration = expiration.AddDate(1, 0, 0)
    setCookie(util.XSRFCookie, appUser.FormToken, expiration, writer)
}

// Clears any data associated with the session. Currently only the XSRF
// cookie.
func ClearSession(writer http.ResponseWriter, request *http.Request) {
    _, myUser, _ := LoggedInUser(writer, request)

    if myUser != nil {
        // Clear the XSRF cookie
        expiration := time.Now()
        expiration = expiration.AddDate(-1, 0, 0)
        setCookie(util.XSRFCookie, "", expiration, writer)
    }
}

// Sets a cookie with given name, value and expiration time.
func setCookie(name string, value string, expiration time.Time,
    writer http.ResponseWriter) {
    cookie := http.Cookie{
        Name: name,
        Value: value,
        Expires: expiration,
    }
    http.SetCookie(writer, &cookie)
}

// Check if this user is a new user and if so save some app specific data.
func newAccount(context appengine.Context,
    appUser *util.ReadelayUser) error {
    appUser.SignupTime = time.Now()

    // Generate a random token for RSS feed access
    token, err := generateToken()
    if err != nil {
        return err
    }
    appUser.RSSID = *token

    // Publishes the RSS feed by default
    // NOTE: This field is not currently used
    appUser.PublishRSS = true

    // Generating a form token in advance
    token, err = generateToken()
    if err != nil {
        return err
    }
    appUser.FormToken = *token

    // Generating an app token in advance
    // NOTE: This field is not currently used
    token, err = generateToken()
    if err != nil {
        return err
    }
    appUser.AppToken = *token

    key := datastore.NewKey(context, "ReadelayUser", appUser.ID, 0, nil)
    key, err = datastore.Put(context, key, appUser)
    return err
}

// Returns a randomly generated token.
func generateToken() (*string, error) {
    var randStrLength = 64

    bytes := make([]byte, randStrLength)
    n, err := rand.Read(bytes)
    if err != nil || n != randStrLength {
        return nil, errors.New("Could not generate token.")
    }
    hasher := sha256.New()
    hasher.Write(bytes)

    token := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
    return &token, nil
}
